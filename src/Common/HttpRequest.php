<?php

namespace Costin\Restapi\Common;

class HttpRequest
{
    protected static $payload = null;
    protected static $method = '';
    protected static $cookie = [];
    protected static $headers = [];
    protected static $parameter;

    public function __construct()
    {
        self::$method = strtolower($_SERVER['REQUEST_METHOD']);
        self::$parameter = isset($_GET['parameter']) ? $_GET['parameter'] : [];
        self::$cookie = $_COOKIE;
        self::$headers = getallheaders();
        self::$payload = file_get_contents('php://input');
    }

    public static function getMethod()
    {
        return self::$method;
    }

    public static function getParameter()
    {
        return self::$parameter;
    }

    public function cookie($param)
    {
        if (isset(self::$cookie[$param])) {
            return self::$cookie[$param];
        }
        return null;
    }

    public function getHeaders()
    {
        return self::$headers;
    }

    public function getHeader($headerName)
    {
        return self::$headers[$headerName];
    }

    public function getPayload()
    {
        return self::$payload;
    }
}
