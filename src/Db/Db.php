<?php

namespace Costin\Restapi\Db;

use Exception;
use PDO;

class Db
{
    /**
     * @var Db
     */
    private static $instance;

    private $connection;

    private static $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    /**
     * Connection constructor.
     * @param $host
     * @param $dataBase
     * @param $username
     * @param $password
     */
    private function __construct($host, $dataBase, $username, $password)
    {
        try {
            $this->connection = new PDO(
                'mysql:host=' . $host . ';dbname=' . $dataBase,
                $username,
                $password,
                self::$options
            );
        } catch (Exception $e) {
            die('Cannot connect to database!');
        }
    }

    public static function getInstance($host, $dataBase, $username, $password)
    {
        if (!isset(self::$instance) || empty(self::$instance)) {
            self::$instance = new self($host, $dataBase, $username, $password);
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function execute($sql, $args = [])
    {
        if (empty($args)) {
            return self::$instance->getConnection()->query($sql);
        }

        $statement = self::$instance->getConnection()->prepare($sql);
        $statement->execute($args);

        return $statement;
    }
}
