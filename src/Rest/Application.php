<?php

namespace Costin\Restapi\Common;

use Costin\Restapi\Rest\Router;
use Exception;

class Application
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    public function run()
    {
        $router = $this->getRouter();
        $router->setData('container', $this->getContainer());

        try {
            echo $router->execute();

            exit;
        } catch (Exception $e) {
            die('Internal error!');
        }
    }
}
