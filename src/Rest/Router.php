<?php

namespace Costin\Restapi\Rest;

use Exception;

class Router
{
    /**
     * @var Translator
     */
    protected $translator;

    protected $endpointsPath = 'App\\Endpoints\\';

    protected $data = [];

    /**
     * @var string
     */
    protected $targetClassName = '';

    /**
     * @var string
     */
    protected $targetMethodName = '';

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return Endpoint
     * @throws
     */
    public function matchRoute()
    {
        try {
            $this->getTranslator()->parseRequest();
            $this->setData('payload', $this->getTranslator()->getPayload());
            $this->setData('endpoint', $this->getTranslator()->getEndpoint());
            $this->setData('method', $this->getTranslator()->getMethod());
            $this->setData('parameter', $this->getTranslator()->getParameter());
            $this->setData('request', $this->getTranslator()->getParsedRequest());
        } catch (Exception $e) {
            die('Invalid request!');
        }

        $this->setTargetClassName(ucfirst($this->getTranslator()->getEndpoint()));

        // search for the requested method in the global methods path
        if (!class_exists($class = $this->endpointsPath . $this->getTargetClassName())) {
            die('Cannot route request!');
        }

        $object = new $class();

        if (!($object instanceof Endpoint)) {
            die('Invalid endpoint!');
        }

        if (!method_exists($object, $this->getData('method'))) {
            die('Endpoint does not have an executor fo the ' . $this->getData('method') . ' method!');
        }

        return $object;
    }

    public function execute()
    {
        try {
            $object = $this->matchRoute();
            $object->setContainer($this->getData('container'));
            $object->setParameter($this->getData('parameter'));
            $object->setRequest($this->getData('request'));

            $output = call_user_func([$object, $this->getData('method')]);

            return $output;
        } catch (Exception $e) {
            die('Error executing method!');
        }
    }

    /**
     * @return string
     */
    public function getEndpointsPath()
    {
        return $this->endpointsPath;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getData($key)
    {
        return $this->data[$key];
    }

    /**
     * @param $key
     * @param $value
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * @return string
     */
    public function getTargetMethodName()
    {
        return $this->targetMethodName;
    }

    /**
     * @param string $targetMethodName
     */
    public function setTargetMethodName($targetMethodName)
    {
        $this->targetMethodName = $targetMethodName;
    }

    /**
     * @return string
     */
    public function getTargetClassName()
    {
        return $this->targetClassName;
    }

    /**
     * @param string $targetClassName
     */
    public function setTargetClassName($targetClassName)
    {
        $this->targetClassName = $targetClassName;
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }
}
