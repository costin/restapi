<?php

namespace Costin\Restapi\Rest;

use Costin\Restapi\Common\Container;

class Endpoint
{
    /**
     * @var Container
     */
    protected $container;
    /**
     * @var array
     */
    protected $request;
    /**
     * @var string
     */
    protected $parameter;

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param Container $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param array $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @param string $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    protected function formatResponse($response)
    {
        return json_encode($response);
    }
}
