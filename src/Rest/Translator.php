<?php

namespace Costin\Restapi\Rest;

use Costin\Restapi\Common\HttpRequest;

class Translator
{
    /**
     * @var HttpRequest
     */
    protected $request = null;
    protected $parsedRequest = [];
    /**
     * @var string
     */
    protected $payload = '';
    /**
     * one of get, post, put, delete, patch
     * @var string
     */
    protected $method = '';
    protected $parameter;
    /**
     * @var string
     */
    protected $endpoint;

    public function __construct(HttpRequest $request)
    {
        $this->request = $request;
    }

    public function parseRequest()
    {
        $this->payload = $this->request->getPayload();
        $this->method = $this->request->getMethod();
        $this->parameter = $this->request->getParameter();

        if (!isset($_GET['endpoint'])) {
            die('No endpoint specified!');
        }

        $this->endpoint = $_GET['endpoint'];

        if (!empty($this->payload)) {
            if (!$this->isValidRequest($this->payload)) {
                die('Invalid request!');
            }
        }

        $this->parsedRequest = json_decode($this->payload, true);

        return $this;
    }

    public function getParsedRequest()
    {
        return $this->parsedRequest;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param $payload
     * @return bool
     */
    protected function isValidRequest($payload)
    {
        $parsedRequest = json_decode($payload, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            return false;
        }

        if (!is_array($parsedRequest)) {
            return false;
        }

        return true;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getParameter()
    {
        return $this->parameter;
    }
}
